require 'pg'

class FilmRepository

  def initialize
    @con = PG.connect :dbname => 'dosporuncentavo_development', :user => 'rafa', :password => ENV["DB_PASSWORD"]
  end

  def existsFilmWithId(id)
    res = @con.exec "SELECT 1 FROM films WHERE id = " + id
    return res.values.size > 0
  end

  def findFilmTitleById(id)
    res = @con.exec "SELECT name from films where id = " + id
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def findListByNameAndUser(listName, userId)
    res = @con.exec "SELECT id FROM LISTS WHERE USER_ID = " + userId + " AND NAME = '" + listName + "'"
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createList(listName, userId)
    return @con.exec("INSERT INTO LISTS (USER_ID, NAME) VALUES (" + userId + ", '" + listName + "') returning id").getvalue(0,0)
  end

  def createFilm(id, title, year, country, duration, rating, n_ratings, synopsis)
    @con.prepare('insertfilm', 'insert into films (id, name, year, country, duration, a_votes, n_votes, synopsis) values ($1, $2, $3, $4, $5, $6, $7, $8)')
    @con.exec_prepared('insertfilm', [id.to_i, title, year, country, duration, rating, n_ratings, synopsis])
    @con.exec('deallocate insertfilm')
  end

  def findDirectoryByName(name)
    res = @con.exec("select id from directors where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createDirector(name)
    @con.exec("insert into directors (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmDirector(directorId, filmId)
    @con.prepare('insertdirector', "insert into film_director (film_id, director_id) values ($1, $2)")
    @con.exec_prepared('insertdirector', [filmId, directorId])
    @con.exec('deallocate insertdirector')
  end

  def findActoryByName(name)
    res = @con.exec("select id from actors where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createActor(name)
    @con.exec("insert into actors (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmActor(actorId, filmId)
    @con.prepare('insertactor', "insert into film_actor (film_id, actor_id) values ($1, $2) ON CONFLICT(film_id, actor_id) DO NOTHING")
    @con.exec_prepared('insertactor', [filmId, actorId])
    @con.exec('deallocate insertactor')
  end

  def findWriteryByName(name)
    res = @con.exec("select id from writers where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createWriter(name)
    @con.exec("insert into writers (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmWriter(writerId, filmId)
    @con.prepare('insertwriter', "insert into film_writer (film_id, writer_id) values ($1, $2) ON CONFLICT (film_id, writer_id) DO NOTHING")
    @con.exec_prepared('insertwriter', [filmId, writerId])
    @con.exec('deallocate insertwriter')
  end

  def findMusicianyByName(name)
    res = @con.exec("select id from musicians where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createMusician(name)
    @con.exec("insert into musicians (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmMusician(musicianId, filmId)
    @con.prepare('insertmusician', "insert into film_musician (film_id, musician_id) values ($1, $2)")
    @con.exec_prepared('insertmusician', [filmId, musicianId])
    @con.exec('deallocate insertmusician')
  end

  def findPhotographeryByName(name)
    res = @con.exec("select id from photographers where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createPhotographer(name)
    @con.exec("insert into photographers (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmPhotographer(photographerId, filmId)
    @con.prepare('insertphotographer', "insert into film_photographer (film_id, photographer_id) values ($1, $2)")
    @con.exec_prepared('insertphotographer', [filmId, photographerId])
    @con.exec('deallocate insertphotographer')
  end

  def findProduceryByName(name)
    res = @con.exec("select id from producers where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createProducer(name)
    @con.exec("insert into producers (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmProducer(producerId, filmId)
    @con.prepare('insertproducer', "insert into film_producer (film_id, producer_id) values ($1, $2) on conflict (film_id, producer_id) do nothing")
    @con.exec_prepared('insertproducer', [filmId, producerId])
    @con.exec('deallocate insertproducer')
  end

  def findGenreyByName(name)
    res = @con.exec("select id from genres where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createGenre(name)
    @con.exec("insert into genres (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmGenre(genreId, filmId)
    @con.prepare('insertgenre', "insert into film_genre (film_id, genre_id) values ($1, $2)")
    @con.exec_prepared('insertgenre', [filmId, genreId])
    @con.exec('deallocate insertgenre')
  end

  def findTopicyByName(name)
    res = @con.exec("select id from topics where name = '" + name + "'")
    if res.values.size > 0
      return res.values[0][0]
    else
      return nil
    end
  end

  def createTopic(name)
    @con.exec("insert into topics (name) values ('" + name + "') returning id").getvalue(0,0)
  end

  def addFilmTopic(topicId, filmId)
    @con.prepare('inserttopic', "insert into film_topic (film_id, topic_id) values ($1, $2)")
    @con.exec_prepared('inserttopic', [filmId, topicId])
    @con.exec('deallocate inserttopic')
  end

  def existsListFilm(listId, filmId)
    return @con.exec('select 1 from list_film where film_id = ' + filmId + ' and list_id = ' + listId).values.size > 0
  end

  def addListFilm(listId, filmId)
    @con.exec('insert into list_film (film_id, list_id) values (' + filmId + ', ' + listId + ')')
  end

  def closeCon
    @con.close
  end

end
