require 'nokogiri'
require 'open-uri'

class FilmaffinityAPI
  def self.getFilmInfo(filmId)
    page = Nokogiri::HTML(URI.open('https://www.filmaffinity.com/es/film' + filmId + '.html'))
    film = {}
    film["id"] = filmId
    finfo = page.search('dl.movie-info')[0].search('dd')
    film["title"] = finfo[0].text.gsub(/aka/,'').strip
    film["year"] = finfo.at('[itemprop=datePublished]').text.strip
    durationContainer = finfo.at('[itemprop=duration]')
    if durationContainer != nil
        film["duration"] = durationContainer.text.gsub(/ min\./,'').strip
    else
        film["duration"] = ''
    end
    film["country"] = finfo.at('img')['alt']

    # Ratings
    ratingContainer = page.at('#movie-rat-avg')
    if ratingContainer != nil
        film["rating"] = ratingContainer.text.gsub(/\,/, '.').strip
        film["n_ratings"] = page.at('#movie-count-rat').search('span')[0].text.gsub(/\./,'').strip
    else
        film["rating"] = ''
        film["n_ratings"] = ''
    end

    # Synopsis
    synopsisContainer = finfo.at('[itemprop=description]')
    if (synopsisContainer != nil)
        film["synopsis"] = synopsisContainer.text.gsub(/\(FILMAFFINITY\)/,'').strip
    else
        film["synopsis"] = ''
    end

    # Directors
    directors=[]
    finfo.search('[itemprop=director]').each { |directorRow|
        directors.append(directorRow.at('[itemprop=name]').text.gsub(/\(.*\)/, '').gsub(/'/,"''").strip)
    }
    film["directors"] = directors

    # Actors
    actors=[]
    finfo.search('[itemprop=actor]').each { |actorRow|
        actors.append(actorRow.at('[itemprop=name]').text.gsub(/'/,"''").strip)
    }
    film["actors"] = actors

    # Credits
    fheaders = page.search('dl.movie-info')[0].search('dt')
    fTable = page.search('dl.movie-info')[0].search('dd, dt')

    # Writers
    writers=[]
    if fheaders.text.include? 'Guion'
        writerRow = fTable.at('dt:contains("Guion")').next.next.search('.credits')
        if !writerRow.nil? and !writerRow.empty?
          writerRow = writerRow[0].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
          writerRow.split(',').each { |writer|
              writers.append(writer.strip)
          }
        end
    end
    film["writers"] = writers

    # Musicians
    musicians=[]
    if fheaders.text.include? 'Música'
        musicianRow = fTable.at('dt:contains("Música")').next.next.search('.credits')
        if !musicianRow.nil? and !musicianRow.empty?
          musicianRow = musicianRow[0].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
          musicianRow.split(',').each { |musician|
            musicians.append(musician.strip)
          }
        end
    end
    film["musicians"] = musicians

    # Photographers
    photographers=[]
    if fheaders.text.include? 'Fotografía'
        photographerRow = fTable.at('dt:contains("Fotografía")').next.next.search('.credits')
        if !photographerRow.nil? and !photographerRow.empty?
          photographerRow = photographerRow[0].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
          photographerRow.split(',').each { |photographer|
              photographers.append(photographer.strip)
          }
        end
    end
    film["photographers"] = photographers

    # Producers
    producers=[]
    if fheaders.text.include? 'Compañías'
        producerRow = fTable.at('dt:contains("Compañías")').next.next.search('.credits')
        if !producerRow.nil? and !producerRow.empty?
          producerRow = producerRow[0].text.gsub(/'/,"''").gsub(/Coprodu.*\;/,'').gsub(/Distribui.*/,'').gsub(/Productor\:.*/,'').strip.gsub(/\.$/,'')
          producerRow.split(',').each { |producer|
              producers.append(producer.strip)
          }
        end
    end
    film["producers"] = producers

    # Genres
    genres=[]
    finfo.search('[itemprop=genre]').each { |genreRow|
        genres.append(genreRow.text.strip)
    }
    film["genres"] = genres

    # Topics
    topics=[]
    finfo.search('a[href*=movietopic]').each { |topicRow|
        topics.append(topicRow.text.strip)
    }
    film["topics"] = topics
    return film
  end
end
