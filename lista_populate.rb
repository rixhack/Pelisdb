require 'nokogiri'
require 'open-uri'
require 'pg'
require './filmaffinityAPI'
require './filmRepository'
require 'pry'
begin
    filmRepository = FilmRepository.new
    userId = '903532'
    listUrlId = '201'
    listName = 'Pendientes'
    listId = filmRepository.findListByNameAndUser(listName, userId)
    if listId == nil
        listId = filmRepository.createList(listName, userId)
    end
    list_url = 'https://www.filmaffinity.com/es/userlist.php?user_id='+userId+'&list_id='+listUrlId
    doc = Nokogiri::HTML(URI.open(list_url))
    nfilms = doc.at_css('div.movie-count').text.strip.split(' ')[1]
    nPages = (nfilms.to_i-1) / 50 + 1
    puts 'Fetching ' + nfilms + ' films in ' + nPages.to_s + ' pages'
    for i in 17..nPages
            puts i
            doc = Nokogiri::HTML(URI.open(list_url + '&page='+i.to_s))
            doc.search('li[data-movie-id]').each { |filmcard|
                sleep(1)
                id = filmcard['data-movie-id']
                    if !filmRepository.existsFilmWithId(id)
                      puts 'Film with id ' + id + ' not found, fetching info...'
                        filmInfo = FilmaffinityAPI.getFilmInfo(id)
                        filmRepository.createFilm(filmInfo["id"], filmInfo["title"], filmInfo["year"].to_i, filmInfo["country"], filmInfo["duration"].to_i, filmInfo["rating"].to_f,
                          filmInfo["n_ratings"].to_i, filmInfo["synopsis"])
                        puts 'Film ' + filmInfo["title"] + " added to database"

                        # Directors
                        filmInfo["directors"].each { |director|
                          directorId = filmRepository.findDirectoryByName(director)
                          if directorId == nil
                            puts 'Adding director ' + director
                            directorId = filmRepository.createDirector(director)
                          end
                          filmRepository.addFilmDirector(directorId.to_i, id.to_i)
                        }

                        # Actors
                        filmInfo["actors"].each { |actor|
                          actorId = filmRepository.findActoryByName(actor)
                          if actorId == nil
                            puts 'Adding actor ' + actor
                            actorId = filmRepository.createActor(actor)
                          end
                          filmRepository.addFilmActor(actorId.to_i, id.to_i)
                        }

                        ### Credits ###

                        # Writers
                        filmInfo["writers"].each { |writer|
                          writerId = filmRepository.findWriteryByName(writer)
                          if writerId == nil
                            puts 'Adding writer ' + writer
                            writerId = filmRepository.createWriter(writer)
                          end
                          filmRepository.addFilmWriter(writerId.to_i, id.to_i)
                        }

                        # Musicians
                        filmInfo["musicians"].each { |musician|
                          musicianId = filmRepository.findMusicianyByName(musician)
                          if musicianId == nil
                            puts 'Adding musician ' + musician
                            musicianId = filmRepository.createMusician(musician)
                          end
                          filmRepository.addFilmMusician(musicianId.to_i, id.to_i)
                        }

                        # Photographers
                        filmInfo["photographers"].each { |photographer|
                          photographerId = filmRepository.findPhotographeryByName(photographer)
                          if photographerId == nil
                            puts 'Adding photographer ' + photographer
                            photographerId = filmRepository.createPhotographer(photographer)
                          end
                          filmRepository.addFilmPhotographer(photographerId.to_i, id.to_i)
                        }

                        # Producers
                        filmInfo["producers"].each { |producer|
                          producerId = filmRepository.findProduceryByName(producer)
                          if producerId == nil
                            puts 'Adding producer ' + producer
                            producerId = filmRepository.createProducer(producer)
                          end
                          filmRepository.addFilmProducer(producerId.to_i, id.to_i)
                        }

                        # Genres
                        filmInfo["genres"].each { |genre|
                          genreId = filmRepository.findGenreyByName(genre)
                          if genreId == nil
                            puts 'Adding genre ' + genre
                            genreId = filmRepository.createGenre(genre)
                          end
                          filmRepository.addFilmGenre(genreId.to_i, id.to_i)
                        }

                        # Topics
                        filmInfo["topics"].each { |topic|
                          topicId = filmRepository.findTopicyByName(topic)
                          if topicId == nil
                            puts 'Adding topic ' + topic
                            topicId = filmRepository.createTopic(topic)
                          end
                          filmRepository.addFilmTopic(topicId.to_i, id.to_i)
                        }
                    end
                    filmTitle = filmRepository.findFilmTitleById(id)
                    if !filmRepository.existsListFilm(listId, id)
                      filmRepository.addListFilm(listId, id)
                      puts 'Added film ' + filmTitle + ' to user list'
                    end
                }
        end
        #filmRepository.closeCon


rescue PG::Error => e

    puts e.message
ensure
  filmRepository.closeCon if filmRepository
end
