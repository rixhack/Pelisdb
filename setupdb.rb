require 'pg'
Are you sure?
begin
    tableNames = ['films', 'directors', 'actors', 'writers', 'musicians',
        'photographers', 'producers', 'genres', 'topics', 'users',
        'ratings', 'film_topic', 'film_genre', 'film_writer', 'film_director',
        'film_actor', 'film_musician', 'film_producer', 'film_photographer']
    con = PG.connect :dbname => 'films', :user => 'rixhack', :password => ENV["DB_PASSWORD"]
    tableNames.each { |table|
        con.exec('DROP TABLE IF EXISTS ' + table + ' CASCADE')
    }
    con.exec "CREATE TABLE films(id INTEGER PRIMARY KEY,
        name VARCHAR(200), year INT, country VARCHAR(50), duration INT,
        a_votes FLOAT, n_votes INT, synopsis TEXT)"
    con.exec "CREATE TABLE directors(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE actors(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE writers(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE musicians(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE photographers(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE producers(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE genres(id SERIAL PRIMARY KEY,
        name VARCHAR(100) UNIQUE)"
    con.exec "CREATE TABLE topics(id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE)"
    con.exec "CREATE TABLE users(id INTEGER PRIMARY KEY,
        name VARCHAR(200))"
    con.exec "CREATE TABLE ratings(film_id INTEGER REFERENCES films(id),
        user_id INTEGER REFERENCES users(id) ON DELETE CASCADE, rating INT, date_rated DATE,
        PRIMARY KEY (film_id, user_id))"
   con.exec "CREATE TABLE film_topic(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        topic_id INTEGER REFERENCES topics(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, topic_id))"
   con.exec "CREATE TABLE film_genre(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        genre_id INTEGER REFERENCES genres(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, genre_id))"
   con.exec "CREATE TABLE film_writer(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        writer_id INTEGER REFERENCES writers(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, writer_id))"
   con.exec "CREATE TABLE film_director(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        director_id INTEGER REFERENCES directors(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, director_id))"
   con.exec "CREATE TABLE film_actor(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        actor_id INTEGER REFERENCES actors(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, actor_id))"
   con.exec "CREATE TABLE film_musician(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        musician_id INTEGER REFERENCES musicians(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, musician_id))"
   con.exec "CREATE TABLE film_producer(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        producer_id INTEGER REFERENCES producers(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, producer_id))"
   con.exec "CREATE TABLE film_photographer(film_id INTEGER REFERENCES films(id) ON DELETE CASCADE,
        photographer_id INTEGER REFERENCES photographers(id) ON DELETE CASCADE,
        PRIMARY KEY (film_id, photographer_id))"
   # Indexes
   con.exec "CREATE INDEX film_name ON films (name)"
   con.exec "CREATE INDEX actor_name ON actors (name)"
   con.exec "CREATE INDEX director_name ON directors (name)"
   con.exec "CREATE INDEX writer_name ON  writers (name)"
   con.exec "CREATE INDEX musician_name ON musicians (name)"
   con.exec "CREATE INDEX photographer_name ON photographers (name)"
   con.exec "CREATE INDEX producer_name ON producers (name)"

   # Users
   con.exec "INSERT INTO users (id, name) values (903532, 'Rafa')"
   con.exec "INSERT INTO users (id, name) values (644919, 'Víctor')"
   con.exec "INSERT INTO users (id, name) values (641149, 'Lostbreak')"
   con.exec "INSERT INTO users (id, name) values (868457, 'Mesicreadores')"



rescue PG::Error => e

    puts e.message

ensure

    con.close if con

end
