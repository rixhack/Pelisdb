require 'nokogiri'
require 'open-uri'
require 'pg'

begin
    requests = 0
    con = PG.connect :dbname => 'films', :user => 'rixhack', :password => 'ENV["DB_PASSWORD"]'
    doc = Nokogiri::HTML(open('https://www.filmaffinity.com/es/tours.php'))
    requests += 1
    tours = doc.search('a[href*=idtour]')
    tours.each { |tourel|
        tourId = tourel['href'].split('=')[1]
        puts 'Fetching films from tour' + tourId
        doct = Nokogiri::HTML(open('https://www.filmaffinity.com/es/tour.php?idtour=' + tourId))
        requests += 1
        films = doct.search('div.movie-card')
        films.each { |film|
            id = film['data-movie-id']
            res = con.exec "SELECT 1 FROM films WHERE id = " + id
                    if res.values.size == 0
                        fdoc = Nokogiri::HTML(open('https://www.filmaffinity.com/es/film' + id + '.html' ))
                        puts requests
                        finfo = fdoc.search('dl.movie-info')[0].search('dd')
                        title = finfo[0].text.gsub(/aka/,'').strip
                        puts title + ' req: ' + requests.to_s
                        year = finfo.at('[itemprop=datePublished]').text.strip
                        durationContainer = finfo.at('[itemprop=duration]')
                        if durationContainer != nil
                            duration = durationContainer.text.gsub(/ min\./,'').strip
                        else
                            duration = ''
                        end    
                        country = finfo.at('img')['title']
                        #puts fdoc
                        ratingContainer = fdoc.at('#movie-rat-avg')
                        
                        if ratingContainer != nil
                            rating = ratingContainer.text.gsub(/\,/, '.').strip
                            n_ratings = fdoc.at('#movie-count-rat').search('span')[0].text.gsub(/\./,'').strip
                        else
                            rating = ''
                            n_ratings = ''
                        end 
                        
                        #Synopsis
                        synopsisContainer = finfo.at('[itemprop=description]')
                        if (synopsisContainer != nil)
                            synopsis = synopsisContainer.text.gsub(/\(FILMAFFINITY\)/,'').strip
                        else
                            synopsis = ''
                        end     
                        
                            con.prepare('insertfilm', 'insert into films (id, name, year, country, duration, a_votes, n_votes, synopsis) values ($1, $2, $3, $4, $5, $6, $7, $8)')
                            con.exec_prepared('insertfilm', [id.to_i, title, year.to_i, country, duration.to_i, rating.to_f, n_ratings.to_i, synopsis])
                            con.exec('deallocate insertfilm')
                        
                        # Directors
                        finfo.search('[itemprop=director]').each { |directorRow|
                            director = directorRow.at('[itemprop=name]').text.gsub(/\(.*\)/, '').gsub(/'/,"''").strip
                            res = con.exec("select id from directors where name = '" + director + "'")
                            if res.values.size == 0 
                                director_id = con.exec("insert into directors (name) " +
                                + "values ('" + director + "') returning id").getvalue(0,0)
                            else
                                director_id = res.getvalue(0,0)    
                            end
                            exists = con.exec("select 1 from film_director where film_id = " + 
                                id + " and director_id = " + director_id)
                            if exists.values.size == 0    
                                con.prepare('insertdirector', "insert into film_director (film_id, director_id) " +
                                + " values ($1, $2)")
                                con.exec_prepared('insertdirector', [id.to_i, director_id.to_i])
                                con.exec('deallocate insertdirector')
                            end    
                        }
                        
                        # Actors
                        finfo.search('[itemprop=actor]').each { |actorRow|
                            actor = actorRow.at('[itemprop=name]').text.gsub(/'/,"''").strip
                            res = con.exec("select id from actors where name = '" + actor + "'")
                            if res.values.size == 0 
                                actor_id = con.exec("insert into actors (name) " +
                                + "values ('" + actor + "') returning id").getvalue(0,0)                         
                            else
                                actor_id = res.getvalue(0,0)    
                            end
                            exists = con.exec("select 1 from film_actor where film_id = " + id + " and actor_id = " + actor_id)
                            if exists.values.size == 0
                                con.prepare('insertactor', "insert into film_actor (film_id, actor_id) " +
                                + " values ($1, $2)")
                                con.exec_prepared('insertactor', [id.to_i, actor_id.to_i])
                                con.exec('deallocate insertactor')
                            end    
                        }
                        
                        ### Credits ###
                        
                        j = 0
                        fheaders = fdoc.search('dl.movie-info')[0].search('dt')
                        
                        # Writers
                        if fheaders.text.include? 'Guion'
                            writerRow = finfo.search('.credits')[j].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
                            writerRow.split(',').each { |writer|
                                writer = writer.strip
                                res = con.exec("select id from writers where name = '" + writer + "'")
                                if res.values.size == 0
                                    writer_id = con.exec("insert into writers (name) " +
                                    + "values ('" + writer + "') returning id").getvalue(0,0)
                                else
                                    writer_id = res.getvalue(0,0)
                                end
                                exists = con.exec('select 1 from film_writer where film_id = ' +
                                    + id + ' and writer_id = ' + writer_id)
                                if exists.values.size == 0    
                                    con.prepare('insertwriter', "insert into film_writer (film_id, writer_id) " +
                                    + " values ($1, $2)")
                                    con.exec_prepared('insertwriter', [id.to_i, writer_id.to_i])
                                    con.exec('deallocate insertwriter')
                                end
                            }
                            j += 1
                        end    
                        
                        # Musicians
                        if fheaders.text.include? 'Música'
                            musicianRow = finfo.search('.credits')[j].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
                            musicianRow.split(',').each { |musician|
                                musician = musician.strip
                                res = con.exec("select id from musicians where name = '" + musician + "'")
                                if res.values.size == 0
                                    musician_id = con.exec("insert into musicians (name) " +
                                    + "values ('" + musician + "') returning id").getvalue(0,0)
                                else
                                    musician_id = res.getvalue(0,0)
                                end
                                exists = con.exec 'select 1 from film_musician where film_id = ' + id + ' and musician_id = ' + musician_id
                                if (exists.values.size == 0)
                                    con.prepare('insertmusician', "insert into film_musician (film_id, musician_id) " +
                                    + " values ($1, $2)")
                                    con.exec_prepared('insertmusician', [id.to_i, musician_id.to_i])
                                    con.exec('deallocate insertmusician')
                                end    
                            }
                            j += 1
                        end
                           
                        # Photographers
                        if fheaders.text.include? 'Fotografía'
                            photographerRow = finfo.search('.credits')[j].text.gsub(/'/,"''").gsub(/\(.*\)/,'').strip
                            photographerRow.split(',').each { |photographer|
                                photographer = photographer.strip
                                res = con.exec("select id from photographers where name = '" + photographer + "'")
                                if res.values.size == 0
                                    photographer_id = con.exec("insert into photographers (name) " +
                                    + "values ('" + photographer + "') returning id").getvalue(0,0)
                                else
                                    photographer_id = res.getvalue(0,0)
                                end
                                exists = con.exec "select 1 from film_photographer where film_id = " + 
                                    + id + " and photographer_id = " + photographer_id
                                if exists.values.size == 0      
                                    con.prepare('insertphotographer', "insert into film_photographer (film_id, photographer_id) " +
                                    + " values ($1, $2)")
                                    con.exec_prepared('insertphotographer', [id.to_i, photographer_id.to_i])
                                    con.exec('deallocate insertphotographer')
                                end     
                            }
                            j += 1
                        end
                        
                        # Producers
                        if fheaders.text.include? 'Productora'
                            producerRow = finfo.search('.credits')[j].text.gsub(/'/,"''").gsub(/Coprodu.*\;/,'').gsub(/Distribuida por.*/,'').gsub(/Productor\:.*/,'').strip
                            producerRow.split('/').each { |producer|
                                producer = producer.strip
                                res = con.exec("select id from producers where name = '" + producer + "'")
                                if res.values.size == 0
                                    producer_id = con.exec("insert into producers (name) " +
                                    + "values ('" + producer + "') returning id").getvalue(0,0)
                                else
                                    producer_id = res.getvalue(0,0)
                                end
                                exists = con.exec("select from film_producer where film_id = " +
                                    id + " and producer_id = " + producer_id)
                                if exists.values.size == 0    
                                    con.prepare('insertproducer', "insert into film_producer (film_id, producer_id) " +
                                    + " values ($1, $2)")
                                    con.exec_prepared('insertproducer', [id.to_i, producer_id.to_i])
                                    con.exec('deallocate insertproducer')
                                end    
                            }
                            j += 1
                        end
                                              
                        
                        # Genres
                        finfo.search('[itemprop=genre]').each { |genreRow|
                            genre = genreRow.text.strip
                            res = con.exec("select id from genres where name = '" + genre + "'")
                            if res.values.size == 0 
                                genre_id = con.exec("insert into genres (name) " +
                                + "values ('" + genre + "') returning id").getvalue(0,0)                         
                            else
                                genre_id = res.getvalue(0,0)    
                            end
                            con.prepare('insertgenre', "insert into film_genre (film_id, genre_id) " +
                            + " values ($1, $2)")
                            con.exec_prepared('insertgenre', [id.to_i, genre_id.to_i])
                            con.exec('deallocate insertgenre')
                        }
                        
                        
                        # Topics
                        finfo.search('a[href*=movietopic]').each { |topicRow|
                            topic = topicRow.text.strip
                            res = con.exec("select id from topics where name = '" + topic + "'")
                            if res.values.size == 0 
                                topic_id = con.exec("insert into topics (name) " +
                                + "values ('" + topic + "') returning id").getvalue(0,0)                         
                            else
                                topic_id = res.getvalue(0,0)    
                            end
                            con.prepare('inserttopic', "insert into film_topic (film_id, topic_id) " +
                            + " values ($1, $2)")
                            con.exec_prepared('inserttopic', [id.to_i, topic_id.to_i])
                            con.exec('deallocate inserttopic')
                        }
                        end
                    }  
                    }
                    
rescue PG::Error => e

    puts e.message 
    
ensure

    con.close if con
    
end                    
