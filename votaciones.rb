require 'nokogiri'
require 'open-uri'
require 'csv'

userId = '641149' #903532
fileName = 'votacionesLostbreak.csv'
doc = Nokogiri::HTML(open('https://www.filmaffinity.com/en/userratings.php?user_id=' + userId))
nfilms = doc.search('div.count')[0].search('b').text
nPages = (nfilms.to_i-1) / 30 + 1
puts 'Fetching ' + nfilms + ' films in ' + nPages.to_s + ' pages'
CSV.open(fileName, "wb") do |csv|
    csv << ["Nombre", "Año", "Director", "Nota media", "Número de votos", "Votación", "Fecha de votación"]
    for i in 1..nPages

        doc = Nokogiri::HTML(open('https://www.filmaffinity.com/en/userratings.php?user_id=' + userId + '&p='+i.to_s))
        daydivs = doc.search('div.user-ratings-wrapper')

        daydivs.each { |daydiv|
            ratingDateText = daydiv.search('div.user-ratings-header').text
            ratingdivs = daydiv.search('div.user-ratings-movie')
            ratingdivs.each { |ratingdiv|
                title = ratingdiv.search('div.mc-title')[0].search('a').text.strip
                ratingdiv.search('div.mc-title')[0].search('a').remove
                year = ratingdiv.search('div.mc-title').text.gsub(/\(|\)/,"").strip
                overallRating = ratingdiv.search('div.avgrat-box').text
                nVotes = ratingdiv.search('div.ratcount-box').text.strip
                director = ratingdiv.search('div.mc-director')[0].search('a').text
                rating = ratingdiv.search('div.ur-mr-rat').text
                csv << [title, year, director, overallRating, nVotes, rating, ratingDateText]
                #puts title + ',' + year + ',' + director + ',' + overallRating + ',' + nVotes + ',' + rating
            }
        }

    end
end
